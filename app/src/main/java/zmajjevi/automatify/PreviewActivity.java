package zmajjevi.automatify;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import zmajjevi.automatify.Receivers.BootCompleted;
import zmajjevi.automatify.Receivers.Receiver;
import zmajjevi.automatify.ListAdapter;

import zmajjevi.automatify.ListAdapter.customButtonListener;

public class PreviewActivity extends AppCompatActivity implements customButtonListener{

    ListAdapter adapter;
    ArrayList<ViewHolder> dataItems = new ArrayList<ViewHolder>();
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Receiver rec = new Receiver(getApplicationContext());
        String[] dataArray = rec.getAllReceiverNames();
        ArrayList<ViewHolder> dataTemp = new ArrayList<>();
        for (int i = 0; i < dataArray.length; ++i){
            dataTemp.add(new ViewHolder());
            dataTemp.get(i).receiver = dataArray[i];
            dataTemp.get(i).task = rec.getTask(dataArray[i]);
            dataTemp.get(i).switchbutton = rec.isEnabled(dataArray[i]);
        }
        dataItems.addAll(dataTemp);
        listView = (ListView) findViewById(R.id.listView);
        adapter = new ListAdapter(PreviewActivity.this, dataItems);
        adapter.setCustomButtonListener(PreviewActivity.this);
        listView.setAdapter(adapter);

        Button dugme1 = (Button) findViewById(R.id.dugmeAdd);

        dugme1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(PreviewActivity.this, SimpleListViewActivity.class));
            }
        });

        Button dugme2 = (Button) findViewById(R.id.dugmeSelect);

        dugme2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(PreviewActivity.this, MainActivity.class));
            }
        });

        NotificationManager nMN = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        Notification n  = new Notification.Builder(getApplicationContext())
                .setContentTitle("Automatify")
                .setContentText("Running")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .build();
        nMN.notify(0, n);

    }

    @Override
    public void onButtonClickListener(int position, ViewHolder value) {
        Toast.makeText(PreviewActivity.this, "Button click " + value.toString(), Toast.LENGTH_LONG).show();

    }
}

/*

    customButtonListener customListener;

    public interface customButtonListener {
        public void onButtonClickListner(int position, String value);
    }

    public void setCustomButtonListener(customButtonListener listener) {
        this.customListener = listener;
    }

    private Context context;
    private ArrayList<String> data = new ArrayList<String>();

    public ListAdapter(Context context, ArrayList<String> dataItem) {
        super(context, R.layout.content_preview, dataItem);
        this.data = dataItem;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.content_preview, null);
            viewHolder = new ViewHolder();
            viewHolder.receivertext = (TextView) convertView.findViewById(R.id.ReceiverName);
            viewHolder.tasktext = (TextView) convertView.findViewById(R.id.TaskName);
            viewHolder.switchButton = (Switch) convertView.findViewById(R.id.childButton);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final String temp = getItem(position);
        viewHolder.receivertext.setText(temp);
        viewHolder.switchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (customListener != null) {
                    customListener.onButtonClickListner(position,temp);
                }

            }
        });

        return convertView;
    }



    public class ViewHolder {
        public TextView receivertext;
        public TextView tasktext;
        public Switch switchButton;
    }

 */