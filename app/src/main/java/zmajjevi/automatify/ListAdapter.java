package zmajjevi.automatify;

import java.util.ArrayList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import zmajjevi.automatify.Receivers.ListReceivers;
import zmajjevi.automatify.Receivers.Receiver;

public class ListAdapter extends ArrayAdapter<ViewHolder> {
    customButtonListener customListner;

    public interface customButtonListener {
        public void onButtonClickListener(int position, ViewHolder value);
    }

    public void setCustomButtonListener(customButtonListener listener) {
        this.customListner = listener;
    }

    private Context context;
    private ArrayList<ViewHolder> data;

    public ListAdapter(Context context, ArrayList<ViewHolder> dataItem) {
        super(context, R.layout.content_preview, dataItem);
        this.data = dataItem;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(context).inflate(R.layout.content_preview,parent,false);

        ViewHolder currentRow = data.get(position);

        final TextView receiver = (TextView) listItem.findViewById(R.id.ReceiverName);
        receiver.setText(currentRow.receiver);

        TextView task = (TextView) listItem.findViewById(R.id.TaskName);
        task.setText(currentRow.task);

        Switch switchButton = (Switch) listItem.findViewById(R.id.childButton);

        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            }
        });
        Receiver rec = new Receiver(getContext().getApplicationContext());
        switchButton.setChecked(rec.isEnabled(currentRow.receiver));

        final ArrayList<ViewHolder> tmp = this.data;

        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton switchButtonn, boolean isChecked) {
                Receiver rec = new Receiver(context.getApplicationContext());
                if (rec.isEnabled(data.get(position).receiver)){
                    rec.disable(data.get(position).receiver);
                    Log.i("switchButton", data.get(position).receiver + " disabling");
                }
                else{
                    rec.enable(data.get(position).receiver);
                    Log.i("switchButton", data.get(position).receiver + " enabling");
                }
            }
        });

        return listItem;
    }


}
class ViewHolder {
    String receiver;
    String task;
    Boolean switchbutton;
}