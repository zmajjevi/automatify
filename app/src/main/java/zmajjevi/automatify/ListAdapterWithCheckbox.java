package zmajjevi.automatify;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.security.PublicKey;
import java.util.ArrayList;

import zmajjevi.automatify.Receivers.Receiver;

public class ListAdapterWithCheckbox extends ArrayAdapter<ViewHolderCheckbox> {
    ListAdapter.customButtonListener customListner;

    public interface customButtonListener {
        public void onButtonClickListener(int position, ViewHolder value);
    }

    public void setCustomButtonListener(ListAdapter.customButtonListener listener) {
        this.customListner = listener;
    }

    private Context context;
    private ArrayList<ViewHolderCheckbox> data;

    public ListAdapterWithCheckbox(Context context, ArrayList<ViewHolderCheckbox> dataItem) {
        super(context, R.layout.content_actionstotask, dataItem);
        this.data = dataItem;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(context).inflate(R.layout.content_actionstotask,parent,false);

        ViewHolderCheckbox currentRow = data.get(position);

        final TextView receiver = (TextView) listItem.findViewById(R.id.action);
        receiver.setText(currentRow.action);

        final CheckBox checkBox = (CheckBox) listItem.findViewById(R.id.checkbox);
        checkBox.setChecked(currentRow.checkBox);

        final ArrayList<ViewHolderCheckbox> tmp = this.data;

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                tmp.get(position).checkBox = !tmp.get(position).checkBox;
            }
        });

        return listItem;
    }

    public String[] getAllActive(){
        ArrayList<String> tmp = new ArrayList<>();
        for (int i = 0; i < this.data.size(); ++i){
            if (this.data.get(i).checkBox){
                tmp.add(this.data.get(i).action);
            }
        }
        String[] ret = new String[0];
        ret = tmp.toArray(ret);
        return ret;
    }

}

class ViewHolderCheckbox {
    String action;
    Boolean checkBox;
    public ViewHolderCheckbox(String action, Boolean checkBox){
        this.action = action;
        this.checkBox = checkBox;
    }
}