package zmajjevi.automatify.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AirplaneModeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("T", "Airplane mode receiver");
        if (intent.getAction().equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)){
            Receiver rec = new Receiver(context);
            if (rec.isEnabled(ListReceivers.AIRPLANE_MODE)){
                rec.runTask(ListReceivers.AIRPLANE_MODE);
            }
        }
    }
}
