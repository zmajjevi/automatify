package zmajjevi.automatify.Receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import zmajjevi.automatify.MainActivity;
import zmajjevi.automatify.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class BootCompleted extends BroadcastReceiver {
    @Override
    @Deprecated
    public void onReceive(Context context, Intent intent) {
        NotificationManager nMN = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Notification n  = new Notification.Builder(context)
                .setContentTitle("Automatify")
                .setContentText("Running")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .build();
        nMN.notify(0, n);

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Receiver rec = new Receiver(context);
            if (rec.isEnabled(ListReceivers.BOOT_COMPLETED)){
                rec.runTask(ListReceivers.BOOT_COMPLETED);
            }
        }
    }
}
