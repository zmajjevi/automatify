package zmajjevi.automatify.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import static android.support.constraint.Constraints.TAG;

public class WifiReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Receiver rec = new Receiver(context);
        final String action = intent.getAction();
        if (action.equals(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION)) {
            if (intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false)) {
                if(rec.isEnabled(ListReceivers.WIFI_CONNECTED)){
                    rec.runTask(ListReceivers.WIFI_CONNECTED);
                }
            } else {
                if (rec.isEnabled(ListReceivers.WIFI_DISCONNECTED)){
                    rec.runTask(ListReceivers.WIFI_DISCONNECTED);
                }
            }
        }
        int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
        if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())){
            if (WifiManager.WIFI_STATE_ENABLING == wifiState) {
                if (rec.isEnabled(ListReceivers.WIFI_TURNED_ON)){
                    rec.runTask(ListReceivers.WIFI_TURNED_ON);
                }
            }
            else if (WifiManager.WIFI_STATE_DISABLING == wifiState){
                if (rec.runTask(ListReceivers.WIFI_TURNED_OFF));
            }
        }
    }
}

