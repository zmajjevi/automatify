package zmajjevi.automatify.Receivers;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.io.*;

import zmajjevi.automatify.Actions.Task;
import zmajjevi.automatify.Receivers.ListReceivers;


public class Receiver{

    private Context context;

    public Receiver(Context context){
        this.context = context;
    }

    public String[] getAllReceiverNames(){
        ListReceivers[] lista = ListReceivers.values();
        String[] imena = new String[lista.length];
        for (int i = 0; i < lista.length; ++i){
            imena[i] = lista[i].toString();
        }
        return imena;
    }

    public String[] getAllActiveReceivers(){
        List<String> lista = new ArrayList<>();
        Log.i("T", "Pravljenje liste");
        ListReceivers[] listaSvih = ListReceivers.values();
        for (int i = 0; i < listaSvih.length; ++i){
            if (this.isEnabled(listaSvih[i])){
                lista.add(listaSvih[i].toString());
            }
        }
        return lista.toArray(new String[0]);
    }

    public ReceiverStruct read(ListReceivers receiver){
        String task = "";
        String enabled = "false";
        Log.i("T", "enable");
        FileInputStream inputStream;

        try {
            inputStream = context.openFileInput("receiver" + receiver);
            String tmp = "";
            int content;
            Log.i("T", "otvorio input strim");
            while ((content = inputStream.read()) != -1) {
                tmp += (char) content;
            }
            Log.i("T", "Napravio tmp: " + tmp);
            task = tmp.split("\n")[0];
            enabled = tmp.split("\n")[1];
            inputStream.close();
            Log.i("T", "TASK " + task);
            Log.i("T", "enabled: " + enabled);
        }
        catch (FileNotFoundException e){
            Log.i("T", "File not found, using defaults");
            task = "";
            enabled = "false";
        }
        catch (IOException e1){

        }
        return new ReceiverStruct(receiver, task, enabled);
    }

    public void setTask(ListReceivers receiver, String task){
        String enabled;
        enabled = read(receiver).enabled;
        writeToFile("receiver" + receiver, task + "\n" + enabled);
    }

    public void setTask(String receiver, String task){this.setTask(this.getListReceivers(receiver), task);}

    public void enable(ListReceivers receiver){
        String task;
        String enabled;
        Log.i("T", "enable");
        task = read(receiver).task;
        enabled = "true";
        writeToFile("receiver" + receiver, task + "\n" + enabled);
    }

    public void enable(String receiver){
        this.enable(this.getListReceivers(receiver));
    }

    public void disable(ListReceivers receiver){
        String task;
        String enabled;
        Log.i("T", "disable");
        task = read(receiver).task;
        enabled = "false";
        writeToFile("receiver" + receiver, task + "\n" + enabled);
    }

    public void disable(String receiver){
        this.disable(this.getListReceivers(receiver));
    }

    public Boolean runTask(ListReceivers receiver){
        //TODO : run task
        Log.i("T", "Run task " + receiver);
        Task.runTask(this.read(receiver).task, context);
        Toast.makeText(context, "Run task " + receiver, Toast.LENGTH_LONG).show();
        return true;
    }

    public Boolean runTask(String task){
        //TODO: run task
        return true;
    }

    public String getTask(ListReceivers receiver){
        return this.read(receiver).task;
    }

    public String getTask(String receiver){ return this.getTask(this.getListReceivers(receiver));}

    public Boolean isEnabled(ListReceivers receiver){
        String enabled = this.read(receiver).enabled;
        Log.i("T is enabled", "isEnabled: " + enabled.equals("true"));
        if (enabled.equals("true")) return true;
        else return false;
    }

    public Boolean isEnabled(String receiver){ return this.isEnabled(this.getListReceivers(receiver));}

    private ListReceivers getListReceivers(String receiver){
        return ListReceivers.valueOf(receiver);
    }

    private void writeToFile(String fileName, String data){
        FileOutputStream outputStream;

        try {
            Log.i("T", "Pravim fajloutput strim");
            outputStream = context.openFileOutput(fileName, context.MODE_PRIVATE);
            Log.i("T", "Napravljen je fajl output strim");
            outputStream.write(data.getBytes());
            Log.i("T", "Upisano je u fajl");
            outputStream.close();
        } catch (Exception e) {
            Log.i("T", e.toString());
            e.printStackTrace();
        }
        Log.i("writeToFile", data);
    }


}
