package zmajjevi.automatify.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class HeadSetReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra("state")){
            Receiver rec = new Receiver(context);
            if (intent.getIntExtra("state", 0) == 0){
                //headsetConnected = false
                if(rec.isEnabled(ListReceivers.HEADSET_UNPLUGGED)){
                    rec.runTask(ListReceivers.HEADSET_UNPLUGGED);
                }
            } else if (intent.getIntExtra("state", 0) == 1){
                //headsetConnected = true;
                if (rec.isEnabled(ListReceivers.HEADSET_PLUGGED)){
                    rec.runTask(ListReceivers.HEADSET_PLUGGED);
                }
            }
        }
    }
}
