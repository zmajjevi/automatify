package zmajjevi.automatify.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OutgoingCallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (! intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) return;
        Receiver rec = new Receiver(context);
        if (rec.isEnabled(ListReceivers.OUTGOING_CALL)){
            rec.runTask(ListReceivers.OUTGOING_CALL);
        }
    }
}
