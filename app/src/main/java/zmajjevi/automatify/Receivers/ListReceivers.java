package zmajjevi.automatify.Receivers;

public enum ListReceivers{
    WIFI_TURNED_ON,
    WIFI_TURNED_OFF,
    WIFI_CONNECTED,
    WIFI_DISCONNECTED,
    SMS_RECEIVED,
    MOBILE_DATA_ON,
    BOOT_COMPLETED,
    HEADSET_PLUGGED,
    HEADSET_UNPLUGGED,
    OUTGOING_CALL,
    AIRPLANE_MODE,
    POWER_CONNECTED,
    POWER_DISCONNECTED,
    SCREEN_OFF,
    SCREEN_ON
}