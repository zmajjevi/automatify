package zmajjevi.automatify.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ScreenReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("T", "ScreenOff receiver");
        Receiver rec = new Receiver(context);
        if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
            if (rec.isEnabled(ListReceivers.SCREEN_OFF)){
                rec.runTask(ListReceivers.SCREEN_OFF);
            }
        }
        else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
            if (rec.isEnabled(ListReceivers.SCREEN_ON)){
                rec.runTask(ListReceivers.SCREEN_ON);
            }
        }

    }
}
