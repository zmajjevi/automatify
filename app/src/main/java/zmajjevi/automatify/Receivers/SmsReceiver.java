package zmajjevi.automatify.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            Receiver rec = new Receiver(context);
            rec.runTask(ListReceivers.SMS_RECEIVED);
            if (rec.isEnabled(ListReceivers.SMS_RECEIVED)){
                Log.i("T", "SMS receiver received");
                rec.runTask(ListReceivers.SMS_RECEIVED);
            }
        }
    }
}
