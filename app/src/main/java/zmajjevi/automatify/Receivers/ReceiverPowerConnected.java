package zmajjevi.automatify.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReceiverPowerConnected extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)){
            return;
        }
        Receiver rec = new Receiver(context);
        if (rec.isEnabled(ListReceivers.POWER_CONNECTED)){
            rec.runTask(ListReceivers.POWER_CONNECTED);
        }
    }
}
