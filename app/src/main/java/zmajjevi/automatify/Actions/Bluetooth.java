package zmajjevi.automatify.Actions;

import android.bluetooth.BluetoothAdapter;


public class Bluetooth {
    public static void turnOn()
    {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!mBluetoothAdapter.isEnabled())
        {
            mBluetoothAdapter.enable();
        }
    }

    public static void turnOff()
    {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter.isEnabled())
        {
            mBluetoothAdapter.disable();
        }
    }
}