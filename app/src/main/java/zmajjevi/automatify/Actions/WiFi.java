package zmajjevi.automatify.Actions;


import android.content.Context;
import android.net.wifi.WifiManager;



public class WiFi {
    public static void turnOn(Context context)
    {
        WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifi != null) {
            wifi.setWifiEnabled(true);
        }
    }

    public static void turnOff(Context context)
    {
        WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifi != null) {
            wifi.setWifiEnabled(false);
        }
    }
}
