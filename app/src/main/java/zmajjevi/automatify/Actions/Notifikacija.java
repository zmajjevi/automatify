package zmajjevi.automatify.Actions;

import android.app.NotificationManager;
import android.content.Context;

import zmajjevi.automatify.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Notifikacija {
    public static void sendNotification(String title, String text, Context context)
    {
        NotificationManager nMN = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        android.app.Notification n  = new android.app.Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setOngoing(false)
                .setOnlyAlertOnce(true)
                .build();
        nMN.notify(1, n);
    }

}
