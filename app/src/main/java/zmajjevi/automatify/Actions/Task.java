package zmajjevi.automatify.Actions;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FilenameFilter;

import zmajjevi.automatify.Fajl;

public class Task {
    public static void runAction(String action, Context context) {
        if (action.equals("WiFiOn"))
            WiFi.turnOn(context);

        else if (action.equals("WiFiOff"))
            WiFi.turnOff(context);

        else if (action.equals("ScreenOn"))
            Screen.turnOn(context);

        else if (action.equals("ScreenOff"))
            Screen.turnOff(context);

        else if (action.equals("BluetoothOn"))
            Bluetooth.turnOn();

        else if (action.equals("BluetoothOff"))
            Bluetooth.turnOff();

        else if (action.equals("MusicOn"))
            Music.turnOn(context);

        else if (action.equals("MusicOff"))
            Music.turnOff(context);


        Log.i("Task", action);
    }

    public static void runTask(String taskName, Context context)
    {
        String[] task = Fajl.readFromFile("Task" + taskName, context).split("\n");

        for (int i = 0; i < task.length; i++)
            runAction(task[i], context);
    }

    public static void makeTask(String taskName, String[] tasks, Context context)
    {
        String task = "";
        for(int i = 0; i < tasks.length; i++)
        {
            task = task + tasks[i] + "\n";
        }

        Fajl.writeToFile("Task" + taskName, task, context);
    }

    public static void addAction(String taskName, String action, Context context)
    {
        String task = action + "\n";
        Fajl.appendToFile(taskName, task, context);
    }

    public static String[] getActions(String taskName, Context context)
    {
        return Fajl.readFromFile("Task" + taskName, context).split("\n");
    }

    public static String[] getAllActions()
    {
        String[] actions = new String[8];

        actions[0] = "BluetoothOn";
        actions[1] = "BluetoothOff";
        actions[2] = "MusicOn";
        actions[3] = "MusicOff";
        actions[4] = "ScreenOn";
        actions[5] = "ScreenOff";
        actions[6] = "WiFiOn";
        actions[7] = "WiFiOff";

        return actions;
    }

    public static String[] getAllTasks(Context context){
        String[] tmp =  context.getFilesDir().list(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.startsWith("Task");
            }
        });
        for (int i = 0; i < tmp.length; ++i){
            tmp[i] = tmp[i].substring(4);
        }
        return tmp;
    }

}