package zmajjevi.automatify.Actions;

import android.annotation.SuppressLint;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import zmajjevi.automatify.R;

public class Screen {
    private static PowerManager mPowerManager;
    private static PowerManager.WakeLock mWakeLock;

    @SuppressLint("InvalidWakeLockTag")
    public static void turnOn(Context context)
    {
        mPowerManager = (PowerManager) context.getApplicationContext().getSystemService(Context.POWER_SERVICE);
        mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "tag");
        mWakeLock.acquire(0);
    }


    @SuppressLint("InvalidWakeLockTag")
    public static void turnOff(Context context)
    {
        DevicePolicyManager policyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        Log.i("T", "Going to sleep now.");
        policyManager.lockNow();
    }

}
