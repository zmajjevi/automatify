package zmajjevi.automatify.Actions;

import android.app.Activity;
import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;

import zmajjevi.automatify.MainActivity;

public class Music {
    public static void turnOn(Context context)
    {
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY);
        try {
            mAudioManager.dispatchMediaKeyEvent(event);
        }
        catch (Exception e){
            Log.e("Music", "Ovo ne radi");
        }

    }

    public static void turnOff(Context context){
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PAUSE);
        try {
            mAudioManager.dispatchMediaKeyEvent(event);
        }
        catch (Exception e){
            Log.e("Music", "Ovo ne radi");
        }
    }
}
