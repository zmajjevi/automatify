package zmajjevi.automatify;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import zmajjevi.automatify.Actions.Task;
import zmajjevi.automatify.MainActivity;
import zmajjevi.automatify.R;

public class SimpleListViewActivity extends Activity {
    private ListView mainListView;
    private ListAdapterWithCheckbox listAdapter;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actionstotask);
        mainListView = (ListView) findViewById(R.id.list);

        //ArrayList<String> actionsForTasks = new ArrayList<String>();
        //actionsForTasks.addAll(Task.getAllActions());

        //listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Task.getAllActions());

        ArrayList<ViewHolderCheckbox> tmp = new ArrayList<ViewHolderCheckbox>();
        final String [] actions = Task.getAllActions();

        for (int i = 0; i < actions.length; ++i){
            tmp.add(new ViewHolderCheckbox(actions[i], false));
        }

        listAdapter = new ListAdapterWithCheckbox(getApplicationContext(), tmp);

        mainListView.setAdapter(listAdapter);
        /*mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> i, View j, int pozicija, long k){

            }
        });*/

        Button done = findViewById(R.id.dugmeGotovo);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String imeTaska = ((EditText) findViewById(R.id.imeTaska)).getText().toString();

                String[] actionsForTask = new String[]{};

                actionsForTask = listAdapter.getAllActive();

                Task.makeTask(imeTaska, actionsForTask, getApplicationContext());
                finish();
            }
        });

    }
}