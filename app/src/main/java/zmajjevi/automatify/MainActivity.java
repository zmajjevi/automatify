package zmajjevi.automatify;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.List;

import zmajjevi.automatify.Actions.Bluetooth;
import zmajjevi.automatify.Actions.Music;
import zmajjevi.automatify.Actions.Screen;
import zmajjevi.automatify.Actions.Task;
import zmajjevi.automatify.Actions.WiFi;
import zmajjevi.automatify.Receivers.ListReceivers;
import zmajjevi.automatify.Receivers.Receiver;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        Receiver rec = new Receiver(getApplicationContext());
        final Receiver finalrec = rec;


        /*Task.makeTask("testTask", new String[] {"WiFiOn", "BluetoothOn"}, getApplicationContext());
        rec.setTask(ListReceivers.MOBILE_DATA_ON, "testTask");
        rec.enable(ListReceivers.MOBILE_DATA_ON);
        rec.disable("MOBILE_DATA_ON");
        rec.enable("MOBILE_DATA_ON");
        //Music.turnOn(getApplicationContext());
        for (int i = 0; i < rec.getAllReceiverNames().length; ++i){
            Log.i("AllTasksTest", rec.getAllReceiverNames()[i]);
        }
        for (int i = 0; i < rec.getAllActiveReceivers().size(); ++i){
            Log.i("AllActiveReceiversTest", rec.getAllActiveReceivers().get(i));
        }*/

        final Spinner dropdown = findViewById(R.id.spinner1);
        String[] listaAkcija = rec.getAllReceiverNames();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listaAkcija);
        dropdown.setAdapter(adapter);

        final Spinner dropdown2 = findViewById(R.id.spinner2);
        String[] listaAkcija2 = Task.getAllTasks(getApplicationContext());
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listaAkcija2);
        dropdown2.setAdapter(adapter2);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalrec.setTask(dropdown.getSelectedItem().toString(), dropdown2.getSelectedItem().toString());
                Log.i("fab", dropdown.getSelectedItem().toString());
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
        //komentar
    }
}
